//
// Copyright (c) 2016 Jason Pritchard
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//
// LED controller
//
#include "LED.h"
#include <Arduino.h>

LED::LED() :
    m_stripState(LEDS_OFF),
    m_useFade(0),
    m_maxBrightness(BRIGHTNESS_100),
    m_fadeSpeed(FADE_SPEED_NORM),
    m_useDigital(0)
{
//  attach();
}

void LED::attach(int ledPin) {
  m_ledPin = ledPin;
  pinMode(m_ledPin, OUTPUT);
  analogWrite(m_ledPin, 0);
}

void LED::fullOn() {
  if (m_useDigital)
    digitalWrite(m_ledPin, HIGH);
  else
    analogWrite(m_ledPin, m_maxBrightness);
  
}

void LED::on() {
  if (m_stripState == LEDS_ON)
    return;
  
  if (m_useFade) {
    for (int i=0; i<m_maxBrightness; i++) {
      analogWrite(m_ledPin, i);
      delay(m_fadeSpeed);
    }
  }
  
  fullOn();
  m_stripState = LEDS_ON;
}

void LED::fullOff() {
  if (m_useDigital)
    digitalWrite(m_ledPin, LOW);
  else
    analogWrite(m_ledPin, 0);
}

void LED::off() {
  if (m_stripState == LEDS_OFF)
    return;
  
  if (m_useFade) {
    for (int i=m_maxBrightness; i>0; i--) {
      analogWrite(m_ledPin, i);
      delay(m_fadeSpeed);
    }
  }
  
  fullOff();
  m_stripState = LEDS_OFF;
}

void LED::toggle() {
  if (m_stripState == LEDS_OFF)
    this->on();
  else
    this->off();
}

void LED::setMaxBrightness(LEDBrightness brightness) {
  m_maxBrightness = brightness;
}

LEDState LED::getCurrentState() {
  return m_stripState; 
}

void LED::setFadeSpeed(LEDFadeSpeed speed) {
  m_fadeSpeed = speed;
}

void LED::enableFade() {
  m_useFade = 1;
}

void LED::useDigital() {
  m_useDigital = 1;
  m_useFade = 0;
  m_maxBrightness = BRIGHTNESS_100;
}

