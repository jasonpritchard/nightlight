//
// Copyright (c) 2016 Jason Pritchard
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//
// LED controller
//
#ifndef LED_H
#define LED_H

//
// States for the LEDs
//
enum LEDState { LEDS_ON, LEDS_OFF };

//
// Available brightness settings
//
enum LEDBrightness {
  BRIGHTNESS_100 = 255,
  BRIGHTNESS_75  = 230,
  BRIGHTNESS_50  = 192,
  BRIGHTNESS_25  = 128,
};

//
// Available fading speeds for turning the LEDs on and off
//
enum LEDFadeSpeed {
  FADE_SPEED_SLOW = 15,
  FADE_SPEED_NORM = 10,
  FADE_SPEED_FAST = 5,
};

//
// Component for controlling LEDs. To have the fading or  
// brightness features, a MOSFET or similar must be used. 
//
class LED 
{
 public:
  //
  // LED constructor - Initialize the device
  //
  LED();
  
  // LED destructor - nothing special
  ~LED() {};

  //
  // Initializer method for the LED component. It is required
  // to call this method first before doing anything else.
  //
  // @param pin LED analog pin number
  //
  void attach(int);
  
  //
  // Enable fading on the LEDs. By default, the lights are 
  // a basic high/low. This allows for a fading transition
  //
  void enableFade();
  
  //
  // Turn the LEDs on. If enableFade() was called previously, this 
  // will transition at the LEDFadeSpeed for this componenet
  //
  void on();
  
  //
  // Turn the LEDs off. If enableFade() was called previously, this 
  // will transition at the LEDFadeSpeed for this componenet
  //
  void off();
  
  //
  // Toggle the LEDs. If enableFade() was called previously, 
  // this will transition at the LEDFadeSpeed for this componenet
  //
  void toggle();
  
  //
  // Set the maximum brightness for the LEDs
  //
  // @param brightness LEDBrightness setting
  //
  void setMaxBrightness(LEDBrightness);
  
  //
  // Set the rate at which the lights fade on and off
  //
  // @param fadeSpeed LEDFadeSpeed setting
  //
  void setFadeSpeed(LEDFadeSpeed);
  
  //
  // Get the component's current LEDState
  //
  LEDState getCurrentState();
  
  //
  // Configure this to use the digital pins
  //
  void useDigital();
  

 private:
  void fullOn();
  void fullOff();
  
  LEDState m_stripState;
  int m_ledPin;
  int m_useFade;
  int m_maxBrightness;
  int m_fadeSpeed;
  int m_useDigital;

};

#endif // LED_H


