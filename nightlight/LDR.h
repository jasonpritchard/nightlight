//
// Copyright (c) 2016 Jason Pritchard
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//
// LDR component
//
#ifndef LDR_H
#define LDR_H

#include "Stats.h"

#ifndef LDR_SAMPLE_SIZE
#define LDR_SAMPLE_SIZE  20
#endif

// Component for working with LDR inputs. This is extremely
// simple in that it only provides for reading a single pin.
// The only rule is that you must call attach() on the LDR before
// reading values.
//
// This will try to stablilize the reading by taking a few samples
// and averaging them out. The default sample size is 30, but that
// that can be overriden with the LDR_SAMPLE_SIZE define.
//
class LDR 
{
 public:
  //
  // LDR constructor - Initialize an LDR component 
  //
  LDR();

  // LDR destructor - nothing special
  ~LDR() {};

  //
  // Initializer method for the LDR component. It is required
  // to call this method first before doing anything else.
  //
  // @param pin LDR pin number
  //
  void attach(int);

  //
  // Method for reading a value from the LDR. More specifically
  // an average value over LDR_SAMPLE_SIZE number of samples.
  //
  int readValue();

  //
  // Change the delay between samples. Helps with smoothing.
  //
  // @param sampleDelay sample delay in milliseconds
  //
  void setSampleDelay(int);


 private:
  void reset();

  Stats<LDR_SAMPLE_SIZE> m_data;
  int  m_sampleSize;
  int  m_sampleDelay;
  int  m_ldrPin;

};

#endif // LDR_H

