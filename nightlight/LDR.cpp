//
// Copyright (c) 2016 Jason Pritchard
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//
// LDR component
//
#include <Arduino.h>
#include "LDR.h"

LDR::LDR() :
  m_sampleSize(LDR_SAMPLE_SIZE),
  m_sampleDelay(10)
{
  reset();
}

void LDR::attach(int ldrPin) {
  m_ldrPin = ldrPin;
  pinMode(m_ldrPin, INPUT);
  reset();
}

int LDR::readValue() {
  for (int i=0; i<m_sampleSize; i++) {
    int ldr = analogRead(m_ldrPin);
    if (ldr > 1024)
      ldr = 1024;
    else if (ldr < 0)
      ldr = 0;
    
    m_data.addValue(ldr);
    delay(m_sampleDelay);
  }

  return m_data.getAverage();
}

void LDR::setSampleDelay(int sampleDelay) {
  m_sampleDelay = sampleDelay;
}

void LDR::reset() {
  m_data.reset();
}


