//
// Copyright (c) 2016 Jason Pritchard
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//
//
// nightlight
//
#include <avr/sleep.h>
#include <avr/power.h>
#include <avr/interrupt.h>
#include "LDR.h"
#include "LED.h"

// Prototypes
void configureInterrupts(void);
void disableInterrupts(void);
int pirActive(void);
int lowLight(void);

// State functions
void motionDetected(void);
void motionStopped(void);

// Variables and constants
static const int pirPin    =   0; // P0/PB0/PCINT0
static const int ldrPin    =   1; // P1/PB1
static const int stripPin  =   4; // P4/PWM analog out
static const int pirWarmUp =   5; // seconds
static const int ldrCutoff = 220; // min LDR value to allow lights on
static const int ldrSampleDelay = 10;
static const LEDFadeSpeed  fadeSpeed = FADE_SPEED_SLOW;
static const LEDBrightness ledBrightness = BRIGHTNESS_50;

LDR ldr;
LED ledStrip;

// state types
enum State { MOTION_DETECTED, MOTION_STOPPED, NOT_SET };
volatile State currentState = NOT_SET;

// flag to let us know that the PIR needs servicing
volatile int pirNeedServicing = 0;


// board startup
void setup() {
  // configure port defaults
  DDRB   = B00000000;
  PORTB |= B11111111;
  pinMode(pirPin, INPUT);

  // configure components: LDR and LED strip
  ldr.attach(ldrPin);
  ldr.setSampleDelay(ldrSampleDelay);
  
  ledStrip.attach(stripPin);
  ledStrip.enableFade();
  ledStrip.setFadeSpeed(fadeSpeed);
  ledStrip.setMaxBrightness(ledBrightness);

  // wait before setting up interrupt, the PIR
  // sensors take a while to warm up
  for (int i=0; i<pirWarmUp; i++)
    delay(1000);

  // get initial state
  currentState = pirActive() ? MOTION_DETECTED : MOTION_STOPPED;
}


// while(1) {
void loop() {
  
  // only process if woken
  if (pirNeedServicing) {

    // grab the state for comparision
    State previousState = currentState;
    currentState = pirActive() ? MOTION_DETECTED : MOTION_STOPPED;

    // look for state change. only keep working
    // if there is a state change
    if (previousState != currentState) {
      // switch for state isn't ideal, but this state machine
      // is too simple to build a full state machine.
      if      (currentState == MOTION_DETECTED) motionDetected();
      else if (currentState == MOTION_STOPPED)  motionStopped();
    }
    
    // reset PIR servicing flag
    pirNeedServicing = 0;
  }

  // go back to sleep. note that this
  // blocks until an interrupt is received
  lowPowerMode();
}


//
// interrupt handler for the PIR pin
//
ISR(PCINT0_vect) {
  pirNeedServicing = 1;
}


//
//  Get the current PIR state
//
int pirActive() {
  return digitalRead(pirPin);
}


//
// Check to see if we are in low light conditions
//
int lowLight() {
  return (ldr.readValue() < ldrCutoff);
}


//
// motion detected state callback
//
void motionDetected() {
  if (lowLight())
    ledStrip.on();
}


//
// motion stopped state callback
//
void motionStopped() {
  ledStrip.off();
}


//
// This will put the board in low power mode. It's not
// a very deep sleep, but it's good enough for this.
//
void lowPowerMode() {
  // only using PCINT here, so just stick with IDLE
  set_sleep_mode(SLEEP_MODE_IDLE);

  // enable sleep bit
  sleep_enable();
  
  // configure PIR interrupt
  configureInterrupts();

  // put the device to sleep
  sleep_cpu();

  //      
  // -> device is asleep here
  //

  // wake up and disable interrupt
  sleep_disable();
  disableInterrupts();
}


//
// Setup the interrups
//
void configureInterrupts() {
  cli();
  //noInterrupts();
  //attachInterrupt(pirPin, isr_onPirChange, CHANGE);
  
  // setup PCINT0 interrupt
  GIMSK |= (1 << PCIE);
  PCMSK |= (1 << PCINT0);
  
  sei();
  //interrupts();
}


//
// Turn off interrupts
//
void disableInterrupts() {
  cli();
  //detachInterrupt(pirPin);

  // setup PCINT0 interrupt
  GIMSK &= ~(1 << PCIE);
  PCMSK &= ~(1 << PCINT0);
}

