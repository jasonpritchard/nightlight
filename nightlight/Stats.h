//
// Copyright (c) 2016 Jason Pritchard
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//
// Stats
//
#ifndef STATS_H
#define STATS_H

//#include <stddef.h>
#include <inttypes.h>
#include <stdlib.h>


typedef uint16_t stats_t;

//
// Class for collecting basica statistics
// sensor data.
//
template<size_t sampleSize>
class Stats
{
 private:
  stats_t m_data[sampleSize];
  stats_t m_total;
  uint8_t m_index;
  uint8_t m_count;
  stats_t m_max;


 public:
  Stats() {
    reset();
  };

  ~Stats() {};

  //
  // Add some data to the collector to be used
  // for analysis later.
  //
  stats_t addValue(stats_t value) {
    if (value < 0)
      return value;
    
    m_total -= m_data[m_index];
    m_data[m_index] = value;
    m_total += m_data[m_index];
    m_index++;
    
    // circle back around on the index
    if (m_index >= sampleSize)
      m_index = 0;

    // increment the current array size
    if (m_count < sampleSize)
      m_count++;
    
    // check/set max
    if (value > m_max)
      m_max = value;

    return value;
  };


  //
  // Get the average of the current dataset.
  //
  stats_t getAverage() {
    if (m_total == 0) 
      return 0;
    
    return (stats_t)(m_total / m_count);
  };


  //
  // Reset all of the data collection
  //
  void reset(void){
    for (int i=0; i<sampleSize; i++)
      m_data[i] = 0;

    m_total = 0;
    m_index = 0;
    m_count = 0;
    m_max = 0;
  };


  //
  //  XXX: Not complete, don't use this.
  //
#if 0
  stats_t getMode(stats_t maxModal) {
    stats_t max = m_max;
    stats_t freqArr[max];
    stats_t mode = 0;
    
    if (maxModal < m_max) {
      max = maxModal;
    }
    
    //  get the frequency of each reading
    for (int i=0; i<max; i++)
      freqArr[i] = 0;

    for (int i=0; i<sampleSize; i++) {
      stats_t modalVal = m_data[i];
      if (modalVal < max) {
        ++freqArr[ modalVal ];
      }
    }
    
    //  get the most occurring, favoring the higher value
    for (int i=0; i<max; i++) {
      if (freqArr[i] > freqArr[mode]) {
        mode = i;
      }
    }
    
    return mode;
  };
#endif

};

#endif // STATS_H

